-- 1. Daftar pegawai dengan usia lebih dari 50 tahun
SELECT * FROM pegawai WHERE usia >= 50;

-- 2. Kantor ID Cabang dengan pegawai terbanyak
SELECT cabang_id, count(id) FROM pegawai GROUP BY cabang_id ORDER BY count(id) DESC LIMIT 1;

-- 3. Daftar divisi dan jumlah pegawainya
SELECT divisi, count(id) FROM pegawai GROUP BY divisi;