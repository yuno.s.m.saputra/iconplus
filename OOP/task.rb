# Main class sebagai tampilan awal untuk memilih opearasi perhitungan
class Calculate
	def self.chose
		print "Please choose (Rectangle/Circle) = "
		selection = gets.chomp

		if selection.downcase == "rectangle"
			perPanjang = Rectangle.new()
			print "Input width of Rectangle         = "
			perPanjang.width = gets.chomp.to_i
			print "Input height of Rectangle        = "
			perPanjang.height = gets.chomp.to_i
			puts "Area of Rectangle is #{perPanjang.area}"
			puts "Circumference of Rectangle is #{perPanjang.circumference}"
		elsif selection.downcase == "circle"
			lingkaran = Circle.new()
			print "Input radius of Circle           = "
			lingkaran.rad = gets.chomp.to_i
			puts "Area of Circle is #{lingkaran.area}"
			puts "Circumference of Circle is #{lingkaran.circumference}"
		else
			puts "Your choose operation not supported, please try again"
		end
	end
	
	
end

# Super class shape yang isinya untuk inisiasi abstrak variable dan method area serta circumference yang akan dioverride pada circle serta rectangle
class Shape
	attr_accessor :rad, :width, :height
	def initialize()
		@rad
		@width
		@height
	end

	def area()
		return true
	end

	def circumference()
		return true
	end
	
	
end

# Class circle dan rectangle yang menginheritance ke class shape terdiri atas 2 method yang mengoverride method dari shape
class Circle < Shape
	def area()
		return ((22*(@rad.to_f**2))/7).to_f.round(2)
	end

	def circumference()
		return ((22*2*@rad.to_f)/7).to_f.round(2)
	end	
	
end

class Rectangle < Shape
	def area()
		return @width*@height
	end

	def circumference()
		return 2*(@width+@height)
	end
	
end

# Pemanggilan main class dengan methode self
Calculate.chose
