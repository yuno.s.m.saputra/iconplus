class User
	# Init OOP
	def initialize(name, age, address)
		@name = name
		@age = age
		@address = address
	end

	# GetName
	def getName
		@name
	end

	# SetName (case sensitif dan spasi antara simbol "=" berpengaruh)
	def setName=(new_name)
		@name = new_name
	end
	
	def showusername
		puts "Your username is @#{@name}"
	end
end

# user = User.new('Youno', '25', 'Yogyakarta')

# puts user.getName
# user.setName="John Doe"

# ============================================================================== #

# Getter Setter Manual
class Cars
	def initialize()
		@brand
		@type
		@price
	end

	def getBrand
		@brand
	end

	def setBrand=(newBrand)
		@brand = newBrand
	end

	def getType
		@type
	end

	def setType=(newType)
		@type = newType
	end

	def getPrice
		@price
	end

	def setPrice=(newPrice)
		@price = newPrice
	end

end

# mobil = Cars.new()
# print "Pick you cars brand = "
# mobil.setBrand = gets.chomp
# print "Pick you cars type  = "
# mobil.setType = gets.chomp
# print "Pick you cars price = "
# mobil.setPrice = gets.chomp

# puts "Your cars choice is with brands #{mobil.getBrand}, type is #{mobil.getType} and for the price is #{mobil.getPrice}"

# ============================================================================== #

# Getter Setter with Attr Accessor
class CarsWithAttr
	attr_accessor :brand, :type, :price
	def initialize()
		@brand
		@type
		@price
	end
	
end

# mobils = CarsWithAttr.new()
# print "Pick you cars brand = "
# mobils.brand = gets.chomp
# print "Pick you cars type  = "
# mobils.type = gets.chomp
# print "Pick you cars price = "
# mobils.price = gets.chomp

# puts "Your cars choice is with brands #{mobils.brand}, type is #{mobils.type} and for the price is #{mobils.price}"

# ============================================================================== #

# Non static OOP

class Perhitungan
	def self.add(num1, num2)
		num1 + num2
	end
	
end

# puts Perhitungan.add(1, 8)

# ============================================================================== #

# Inheritance

class Town
	def initialize(name, state)
		@name
		@state
	end

	def info
		puts "#{@name} is located in #{@state}"
	end
	
end

class Town1 < Town
	
end

kota = Town1.new("Yogyakarta", "Yogyakarta")
puts kota.info