student = {Alice: 75, Bob: 80, Candra: 100, Dede: 64, Eka: 90}

# Method menentukan nilai rata" dari semua siswa
def average(student)
	sum = 0
	student.each do |key, val|
		sum = sum + val
	end
	return sum.to_f/student.length.to_f
end

# Method untuk mendata siswa yang memiliki nilai diatas rata" nilai keseluruhan siswa
def aboveAverage(avg, student)
	cnt = 0
	list = []
	student.each do |key, val|
		if(val > avg)
			cnt = cnt + 1
			list << "   #{key} with grade #{val}"
		end
	end
	return list
end

# Method untuk mengkonfersi nilai kedalam huruf
def convertGrade(student)
	i = 1
	student.each do |key, val|
		if(val >= 80)
			puts "   #{i}. #{key} with grade #{val} get alphabeth score for 'A'"
		elsif (val >= 70)
			puts "   #{i}. #{key} with grade #{val} get alphabeth score for 'B'"
		elsif (val >= 60)
			puts "   #{i}. #{key} with grade #{val} get alphabeth score for 'C'"
		elsif (val >= 40)
			puts "   #{i}. #{key} with grade #{val} get alphabeth score for 'D'"
		else
			puts "   #{i}. #{key} with grade #{val} get alphabeth score for 'E'"
		end
		i = i + 1
	end
end

# Method untuk menentukan hasil akhir
def result(student)
	puts "1. Class average grade is #{average(student)}"	
	puts "   And list students with above average grade is :"
	puts aboveAverage(average(student), student)
	puts ""
	puts "2. Alphabet grade of each student is :"
	convertGrade(student)
end

result(student)