name = ["Jono", "Budi", "Rudi"]

for i in name do
	puts i	
end

name.each do |val|
	puts "Foreach : #{val}"
end

x = 10
while x >= 1
	puts "Number for - #{x}"
	x = x - 2
end