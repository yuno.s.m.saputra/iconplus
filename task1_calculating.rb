# Method menentukan hasil akhir
def calculation(operation=nil, num1, num2)
	if operation
		if operation == "addition" || operation == ""
			"The result is #{addition(num1, num2)}"
		elsif operation == "substraction"
			"The result is #{substraction(num1, num2)}"
		elsif operation == "multiplication"
			"The result is #{multiplication(num1, num2)}"
		elsif operation == "division"
			"The result is #{division(num1, num2)}"
		elsif operation == "modulus"
			"The result is #{modulus(num1, num2)}"
		else
			puts "Your input operation not supported, please try again."
		end		
	else
		"The result is #{addition(num1, num2)}"
	end
end

# Method penjumlahan
def addition(num1, num2)
	res = num1+num2
	return res
end

# Method pengurangan
def substraction(num1, num2)
	res = num1-num2
	return res
end

# Method perkalian
def multiplication(num1, num2)
	res = num1*num2
	return res
end

# Method pembagian
def division(num1, num2)
	res = num1.to_f/num2.to_f
	return res.to_f
end

# Method modulus
def modulus(num1, num2)
	res = num1%num2
	return res
end

# Inputan angka 1, angka 2 dan operasi yang akan dijalankan
print "Input first number (empty will be 0)   : "
first = gets.chomp.to_i

print "Input second number (empty will be 0)  : "
second = gets.chomp.to_i

print "Input calculation type (addition/substraction/multiplication/division/modulus)  (empty will be default as addition)    : "
operation = gets.chomp

puts calculation(operation, first, second)