nama = ["Ahmad", "Tono", "Tini", "Bambang", "Agus", "Agung"]

# Mehod hasil akhir
def result(nama)
	puts "1. Name length is Odd and start with letter 'A' : "
	letterAOdd(nama)
	puts ""
	puts "2. Name start with letter 'T' : "
	letterT(nama)
	puts ""
	puts "3. Name with other Rule : "
	other(nama)
end

#Method menentukan nama dengan awalan huruf A dan dengan jumlah karakter ganjil 
def letterAOdd(nama)
	nama.each do |val|
		if val.size % 2 == 1 && val[0,1] == "A"
			puts val.upcase
		end
	end
end

# Methode menentukan nama dengan awalan huruf T
def letterT(nama)
	nama.each do |val|
		if val[0,1] == "T"
			puts val.downcase
		end
	end
end

# Method menentukan nama selain dari dua rule diatas
def other(nama)
	nama.each do |val|
		if ((val.size % 2 == 1 && val[0,1] != "A") || (val.size % 2 == 0 && val[0,1] == 'A')) && val[0,1] != "T"
			puts val.reverse.capitalize
		end
	end
end

result(nama)