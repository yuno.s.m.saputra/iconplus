# frozen_string_literal: true

require 'faker'

# Class Emoney untuk melakukan transaksi virtual dengan metode e-money
class Emoney
  attr_accessor :id, :saldo, :is_active, :pil
  def initialize
    @id = Faker::Number.number(digits: 6)
    @saldo = 0
    @is_active = false
    @pil = 0
  end

  def transaction(nominal)
    if @saldo >= nominal
      @saldo -= nominal
      puts "Saldo anda: #{@saldo}"
    else
      puts 'Saldo tidak mencukupi. Silakan top-up dahulu.'
      puts ''
    end
  end

  def cek_saldo
    puts ''
    puts "Saldo anda adalah #{@saldo}"
    puts ''
  end

  def aktivasi
    @is_active = true if @is_active == false
    puts ''
  end

  def topup(nominal)
    @saldo += nominal
    puts "Saldo: #{@saldo}"
    puts ''
  end

  def menu
    puts ''
    puts 'E-Money System'
    puts '===================='
    puts '1. Cek Saldo'
    puts '2. Aktivasi'
    puts '3. Top Up'
    puts '4. Transaksi'
    puts '==================='
    print 'Masukkan pilihan anda = '
    @pil = gets.chomp.to_i
  end
end

emoney = Emoney.new
flag = false
while flag == false
  emoney.menu
  if emoney.pil == 1
    emoney.cek_saldo
    print 'Coba Lagi ?(y/t) = '
    cb = gets.chomp
    flag = true if cb != 'y'
  elsif emoney.pil == 2
    emoney.aktivasi
    print 'Coba Lagi ?(y/t) = '
    cb = gets.chomp
    flag = true if cb != 'y'
  elsif emoney.pil == 3
    if emoney.is_active
      print 'Nominal Top Up = '
      nom = gets.chomp.to_f
      emoney.topup(nom)
    else
      puts ''
      puts 'E-money belum aktif'
      puts ''
    end
    print 'Coba Lagi ?(y/t) = '
    cb = gets.chomp
    flag = true if cb != 'y'
  elsif emoney.pil == 4
    if emoney.is_active
      print 'Nominal Transaksi = '
      nom = gets.chomp.to_f
      emoney.transaction(nom)
    else
      puts ''
      puts 'E-money belum aktif'
      puts ''
    end
    print 'Coba Lagi ?(y/t) = '
    cb = gets.chomp
    flag = true if cb != 'y'
  else
    puts 'Terima Kasih'
  end
end
# EOF
